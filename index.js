function validate(formSelector) {
    const inputs = document.querySelectorAll(`${formSelector} input`);
    let valid = true;

    inputs.forEach(input => {
        if (input.hasAttribute('rule-required')) {
            if (input.value) {
                input.classList.remove('is-invalid');
                input.parentElement.querySelector('.invalid-feedback').classList.add('hidden');
            } else {
                input.classList.add('is-invalid');
                input.parentElement.querySelector('.invalid-feedback').classList.remove('hidden');
                valid = false;
            }
        }
    });

    return valid;
}

function Contact(name, surname, phone) {
    this.name = name;
    this.surname = surname;
    this.phone = phone;
    this.id = Date.now();
}

const inputValue = selector => document.querySelector(selector).value;

const setInputValue = (selector, value) => document.querySelector(selector).value = value;

const getContacts = () => localStorage.getItem('contacts') ? JSON.parse(localStorage.getItem('contacts')) : [];

function clearForm(formSelector) {
    const inputs = document.querySelectorAll(`${formSelector} input`);

    inputs.forEach(input => input.value = '');
}

function addContact() {
    const formSelector = '#contacts-form';
    const isValid = validate(formSelector);

    if (isValid) {
        const contacts = getContacts();
        const contact = new Contact(inputValue('#name'), inputValue('#surname'), inputValue('#phone'));
        clearForm(formSelector);
        contacts.push(contact);
        localStorage.setItem('contacts', JSON.stringify(contacts));
        renderTable();
    }
}

function toggleFormButtons() {
    document.getElementById('btn-add').classList.toggle('hidden');
    document.getElementById('btn-update').classList.toggle('hidden');
}

function updateContact() {
    const formSelector = '#contacts-form';
    const isValid = validate(formSelector);

    if (isValid) {
        const code = inputValue('#code');
        const contacts = getContacts();
        const contact = contacts.filter(c => c.id == code)[0];
        contact.name = inputValue('#name');
        contact.surname = inputValue('#surname');
        contact.phone = inputValue('#phone');

        // remove old contact version
        let updatedContacts = contacts.filter(c => c.id != code);
        // add updated contact version
        updatedContacts.push(contact);

        clearForm(formSelector);
        localStorage.setItem('contacts', JSON.stringify(updatedContacts));
        renderTable();

        toggleFormButtons();
    }
}

function deleteContact(id) {
    const contacts = getContacts();
    const filteredContacts = contacts.filter(contact => contact.id != id);
    localStorage.setItem('contacts', JSON.stringify(filteredContacts));
    renderTable();
}

function editContact(code) {
    const contacts = getContacts();
    const {name, surname, phone, id} = contacts.filter(c => c.id == code)[0];

    setInputValue('#name', name);
    setInputValue('#surname', surname);
    setInputValue('#phone', phone);
    setInputValue('#code', id);

    toggleFormButtons();
}

function renderTable() {
    const contacts = getContacts();
    const tbody = document.querySelector('#contacts-table tbody');

    while (tbody.firstChild) {
        tbody.removeChild(tbody.firstChild);
    }

    for (let i = 0; i < contacts.length; i++) {
        const {name, surname, phone, id} = contacts[i];
        const tr = document.createElement('tr');
        tr.innerHTML = `
<td>${i + 1}</td>
<td>${name}</td>
<td>${surname}</td>
<td>${phone}</td>
<id>
<button type="button" class="btn btn-primary" onclick="editContact(${id})">Edit</button>
<button type="button" class="btn btn-danger" onclick="deleteContact(${id})">Delete</button>
</id>
`;
        tbody.appendChild(tr);
    }
}

window.onload = () => renderTable();